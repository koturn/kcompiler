import java.io.*;


%%


%public
%class      Lexer
%implements Parser.yyInput

%type int
%eofval{
  return YYEOF;
%eofval}


%{
    private int token;
    @Override
    public boolean advance() throws java.io.IOException {
        token = yylex();
        return token != YYEOF;
    }
    @Override
    public int token() {
        return token;
    }
    @Override
    public Object value() {
        return value;
    }
    Object value;  // 字句規則部でここに値を代入する
%}


digit = [0-9]
other = .


%%


"+"        { value = new Character('+'); return Parser.ADDOP; }
"-"        { value = new Character('-'); return Parser.ADDOP; }
"*"        { value = new Character('*'); return Parser.MULOP; }
"/"        { value = new Character('/'); return Parser.MULOP; }
"%"        { value = new Character('%'); return Parser.MULOP; }
"print"    { return Parser.PRINT; }
{digit}+   { value = new Integer(yytext()); return(Parser.NUMBER); }
[ \t]      { }
\n|\r|\r\n { return Parser.EOL; }
{other}    { return yytext().charAt(0); }
