import java.io.*;

public class KCompiler {
    public static void main(String[] args) {
        if (args.length != 1) {
            showUsage();
            return;
        }
        try {
            FileReader sourceFile = new FileReader(args[0]);
            Lexer lexer = new Lexer(sourceFile);
            Parser yyparser = new Parser();

            defineClass("KCTest", "public", "java/lang/Object");
            initClass();
            startMain(5);
            yyparser.yyparse(lexer);
            endMain();
        } catch (Parser.yyException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void showUsage() {
        System.err.println("引数は1つにしてください。");
        System.err.println("KCompiler [source-file]");
    }

    private static void defineClass(String className, String access, String superClass) {
        System.out.println(".class " + access + " " + className);
        System.out.println(".super " + superClass);
    }

    private static void initClass() {
        System.out.println(".method public <init>()V");
        System.out.println("  aload_0");
        System.out.println("  invokenonvirtual java/lang/Object/<init>()V");
        System.out.println("  return");
        System.out.println(".end method");
    }

    private static void startMain(int stackSize) {
        System.out.println(".method public static main([Ljava/lang/String;)V");
        System.out.println("  .limit stack " + stackSize);
    }

    private static void endMain() {
        System.out.println("  return");
        System.out.println(".end method");
    }
}

