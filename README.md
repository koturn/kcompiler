KCompiler
====

## Summary:
This is koturn's compiler implemented with java.
At present, this is an experimental program.


## Usage
To make a java assembler code, Execute following command.

```sh
$ java -jar KCompiler.jar source.txt > source.j
```

And execute following command to assemble source.j.
```sh
$ java -jar jasmin.jar source.j
```


## Build:
Use ant or gmake.

### Ant:
```sh
$ ant
```

### gmake:
```sh
$ make
```


## Requirement:
- [JFlex(The Fast Scanner Generator for Java)](http://jflex.de/)
- [jay(a yacc for Java)](http://www.informatik.uni-osnabrueck.de/alumni/bernd/jay/)
- [Jasmin](http://jasmin.sourceforge.net/)
